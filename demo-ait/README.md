**WORK UNDERWAY. The organisation of the AIT procedures evolves with time and may be subject to changes (hopefully not for those already written). Deployment on custom projects is currently being worked on.**

Your project's AIT folder (we're going to call it 'yourProj-ait') shall first be created on your Gitlab server and cloned on your machine. An authentification key should also be created, by going in the project's gitlab Settings/Acces Tokens. **Beware** copy and save the key in a file, as it will dissapear when leaving the page.

The AIT folders shall follow the same structure as the one given in the ait_tools/demo-ait repo (also detailed in part 0.1. below).

## 1. Folder organisation

At minimum, the repo shall contain the following folders and your project's parts Catalog:

```
yourProj-ait
	|--0_Organisation
	|	|-- yourProj_Catalog.xlsx
	|
	|--1_Parts
	|   	|
	|
	|--2_Assemblies
	|   	|
	|
	|--README.md
```

Once yourProject-ait is cloned, a config file shall be created in the ait_tools/scripts folder, under the name 'yourProject-ait_config.ini' and contain the same information as in the demo-ait_config.ini file.

## 2. AIT Documentation

This is an guide of how the AIT respository shall be organised and how to properly store and name documents. Documents stored on the ait repo are linked to certain parts, with their references corresponding to the one found in the document 'YourProj_Catalog.xlsx' OR the 'YourProj_Catalog.json'.

- **Spacecraft catalog**: a list of all sub-assemblies and main parts (ie: boards, antennas, specific mechanical parts etc)
- **Test Matrix**: defines the tests, on what part or sub-assembly and when it is done in the AIT process.
- **AIT sequence diagram**: includes all the steps of the AIT process, for each sub-assembly.
- **Logbooks**: documents any actions performed on a part or sub-assembly, with links to its documentation (data-sheet, and all the docs listed below).
- **Reception control**: packaging control, taking pictures of the part, storing the data-sheet.
- **Test and Assembly as-runs**: each operation performed on a part during a test is documented in an as-run for the appropriate procedure. Pictures are stored in the document.
- **Trending**: a trending analysis is performed to ensure there is no drift during the testing sequence (eg: voltage of signal).
- **Anomaly list and reports**: a report that includes the anomalies, their causes, investigation and solutions, for each part or sub-assembly.



## 3. Document naming convention

Each document associated to a part (logbooks, reception control, asruns, trendings, anomaly reports) should be named in the following fashion:

- Logbook: matcode_Logbook.md
- Reception control: PCVE_matcode_yyyy_mm_dd.pdf, matcode_RecepIMG_imagenb.jpg
- Equipment validation: EquipValid_matcode_partRef.pdf

further conventions will be added.

with the **matcode** being the 6 digit Material code, the **partRef** being the reference name for the part, both found in the Catalog.
Furthermore, when commiting a doc on the remote, the commit message should always include a tag under the form [tag], for example: [doc], [admin], [yourtask]. 

```
cd yourProj-ait
git add yourDocName
git commit -am "[doc] adding this new document about something"
git push origin master
```

### Finding the files associated to a commit

Each part logbook contains an id associated to the commit. When clicking this id, the documents associated to the commit are shown and can be accessed, in the state they were at the time of the commit.

