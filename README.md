@author: Gerald Imhof



This is an guide of how to organise your AIT folder and use the AIT semi-automation tools. Documents stored in your project's AIT repo are linked to certain parts, with their references found in the spacecraft's parts catalog. The tools here help manage the documents by using the information stored in the catalog, which is one of the main interfaces for the AIT operator.

# 0. Preparations

**WORK UNDERWAY. The organisation of the AIT procedures evolves with time and may be subject to changes (hopefully not for those already written). Deployment on custom projects is functional.**

Your project's AIT folder (we're going to call it 'yourProj-ait') shall first be created on your Gitlab server and cloned on your machine. An authentification key should also be created, by going in the project's gitlab Settings/Acces Tokens. **Beware** copy and save the key in a file, as it will dissapear when leaving the page.

The AIT folders shall follow the same structure as the one given in the ait_tools/demo-ait repo (also detailed in part 0.1. below).

## 0.1. Folder organisation

At minimum, the repo shall be a copy of the demo-ait folder:

```
yourProj-ait
	|--0_Organisation
	|	|-- templates
	|	|-- yourProj_Catalog.xlsx
	|
	|--1_Parts
	|   	|
	|
	|--2_Assemblies
	|   	|
	|
	|--README.md
```

Once yourProject-ait is cloned, create a folder to locally store the semi-automation scripts (different from the ait-tool/script folder).

eg:

```
/home/user/AIT/ait-tools/

/home/user/AIT/scripts/
```

A config file shall be created in your local scripts folder, under the name 'yourProject-ait_config.ini' and contain the same information as in the demo-ait_config.ini file. In addition, modify the ait_tools_local_path.ini file so that your repo ./ait-tools path corresponds to your local folder.

## 0.2. AIT Documentation

This is an guide of how the AIT respository shall be organised and how to properly store and name documents. Documents stored on the ait repo are linked to certain parts, with their references corresponding to the one found in the document 'YourProj_Catalog.xlsx' OR the 'YourProj_Catalog.json'.

- **Spacecraft catalog**: a list of all sub-assemblies and main parts (ie: boards, antennas, specific mechanical parts etc)
- **Test Matrix**: defines the tests, on what part or sub-assembly and when it is done in the AIT process.
- **AIT sequence diagram**: includes all the steps of the AIT process, for each sub-assembly.
- **Logbooks**: documents any actions performed on a part or sub-assembly, with links to its documentation (data-sheet, and all the docs listed below).
- **Reception control**: packaging control, taking pictures of the part, storing the data-sheet.
- **Test and Assembly as-runs**: each operation performed on a part during a test is documented in an as-run for the appropriate procedure. Pictures are stored in the document.
- **Trending**: a trending analysis is performed to ensure there is no drift during the testing sequence (eg: voltage of signal).
- **Anomaly list and reports**: a report that includes the anomalies, their causes, investigation and solutions, for each part or sub-assembly.

## 0.3. Document naming convention

Each document associated to a part (logbooks, reception control, asruns, trendings, anomaly reports) should be named in the following fashion:

- Logbook: matcode_Logbook.md
- Reception control: PCVE_matcode_yyyy_mm_dd.pdf, matcode_RecepIMG_imagenb.jpg
- Equipment validation: EquipValid_matcode_partRef.pdf

further conventions will be added.

with the **matcode** being the 6 digit Material code, the **partRef** being the reference name for the part, both found in the Catalog.
Furthermore, when commiting a doc on the remote, the commit message should always include a tag under the form [tag], for example: [doc], [admin], [yourtask]. 

```
cd yourProj-ait
git add yourDocName
git commit -am "[doc] adding this new document about something"
git push origin master
```

### Finding the files associated to a commit

Each part logbook contains an id associated to the commit. When clicking this id, the documents associated to the commit are shown and can be accessed, in the state they were at the time of the commit.
 

# 1. Creating the AIT conda environment

To install anaconda, follow the instructions from this page:
https://digestize.medium.com/how-to-install-conda-in-ubuntu-and-create-an-environment-4917c29eb839

### On Linux:

To run the automation scripts, the following environement has to be created:

```
conda create -n ait -c anaconda python=3.9.7
```
```
conda activate ait
```

and the following package to be installed via command line:
 
```
sudo apt install python3-pip
pip install --upgrade python-gitlab==3.4.0 argparse==1.4.0 openpyxl==3.0.9 progress==1.6  pandas==1.4.2 configparser==5.2.0
```


# 2. Semi-automation scripts

In your local scripts/ folder, three python scripts allow the creation of the parts folders and log the commits associated to the parts. The prerequisites for this to work is to have the yourProj-ait clone on your PC, as the scripts here perform their actions on the ait local folder. It is strongly recommended that your ait repo follows the same structure as the demo-ait one (found in section 0.2.).

Before running the scripts, make sure that you followed the instructions in the 0. Preparations section. You also need to work in the ait conda environment.
```
conda activate ait
```


## AIT_catalogJSON.py

This script saves the information in the Catalog into a json format. This json file is the vector for all the information needed to organise the work.
```
cd scripts
python3 AIT_catalogJSON.py -p yourProj
```

## AIT_createFolders.py

From the Catalog.json file created by the previous script, this script automatically creates all part folders that have been given a Material code in the excel sheet. The folder names follow the same convention 'partRef_matcode_systemcode' and are stored in the /1_Parts folder.
```
cd scripts
python3 AIT_createFolders.py -p yourProj
```

## AIT_autoLog_Parts.py and AIT_autoLog_Assemnlies.py

The AIT_autoLog_Parts.py and Assemblies scripts fetch for all actions performed on all parts and puts them in the associated Logbook. Running the script should be performed regularly. The user simply needs to run the script in the ait environment, with their private authentification key as an argument (other optional arguments exist).


```
cd scripts
python3 AIT_autoLog_Parts.py -p yourProj
```

## AIT_MatcodeGen.py

This script autogenerates material with 6 digits, verifies that they don't already exist in the Matcode sheet, and adds the code to the list in the Part_Codes.xlsx sheet, stored in the ait-tools/0_Organisation/ folder.

```
cd scripts
python3 AIT_MatcodeGen.py
```


