# Written by Gerald Imhof, DCAS


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
import argparse
import math
import os
import pandas as pd
import random as rand
import configparser

parser = argparse.ArgumentParser()

parser.add_argument('-n',
                    "--numbertoGenerate",
                    help="Choose the number of material codes you wish to generate.")

args = parser.parse_args()

numbertoGenerate = args.numbertoGenerate
    
    
config_obj = configparser.ConfigParser()

config_obj.read("ait_tools_local_path.ini")

path = config_obj["local_path"]

ait_tools_local_path = path["ait_tools_local_path"]

#ScriptPath = os.path.realpath(__file__)

#RepoPath = ScriptPath.replace("scripts/AIT_MatcodeGen.py", "")

xlsx_file = ait_tools_local_path + "0_Organisation/Part_Codes.xlsx"

for i in range(0,int(numbertoGenerate)):

    if os.path.exists(xlsx_file):
    
        CodeSheet = pd.read_excel(xlsx_file,sheet_name='Used_Matcode')
        
        nb_usedcodes = len(CodeSheet.Used_Matcode)
        
        IdleCodes = []
        
        UsedCodes = []
        
        for usedcode in range(0,nb_usedcodes):
            
            if math.isnan(CodeSheet.Used_Matcode[usedcode]) is False:
                
                UsedCodes.append(int(CodeSheet.Used_Matcode[usedcode]))
            
            if math.isnan(CodeSheet.Idle_Matcode[usedcode]) is False:
                
                IdleCodes.append(int(CodeSheet.Idle_Matcode[usedcode]))
        
        
        codeBase=[]
            
        for code in range(100000,1000000):
            codeBase.append(code)
        
        
        out=0
        
        while out!=1:
            
            idx = rand.randint(0,len(codeBase)-1)
            
            if codeBase[idx] in UsedCodes:
                
                pass
            
            elif codeBase[idx] not in UsedCodes and codeBase[idx] not in IdleCodes:
                
                out = 1
                
                newcode = codeBase[idx]
                newcodeList = {'Idle_Matcode':newcode}
                
                print("Adding new code to the Part_Codes sheet")
                
                CodeSheet = CodeSheet.append(newcodeList, ignore_index=True)
                
                with pd.ExcelWriter(xlsx_file, if_sheet_exists = 'replace',mode='a') as writer:
                    CodeSheet.to_excel(writer,sheet_name='Used_Matcode',index=False)
    
                print(newcode)
                
        
    else:
            
        print("Please create or find the Part_Codes.xlsx file and place it in the ' ../ait_tools/0_0rganisation ' folder.")


    



