# Written by Gerald Imhof, DCAS

import json
import argparse
import os
import configparser
import sys

# Parser to select the project config file to work from

parser = argparse.ArgumentParser()

parser.add_argument('-p',
                    "--Project",
                    help="Entre the project's name. The config file of this project should be created before executing this script and named 'YourProject_config.ini'. ")

args = parser.parse_args()

# Import project info from config file

config_obj = configparser.ConfigParser()

config_obj.read((args.Project+"-ait_config.ini"))
projInfo = config_obj["project_info"]
projFiles = config_obj["project_files"]

ServUrl = projInfo["ServUrl"]
ProjName = projInfo["ProjName"]
RepoPath = projInfo["LocalRepoPath"]
Authkey = projInfo["Authkey"]

Catalogxlsx_file = projFiles["Catalog"]
Catalogjson_file = projFiles["CatalogJSON"]
Assemblies_file = projFiles["Assemblies"]


ScriptPath = os.path.realpath(__file__)


OrgFolder = RepoPath + "/0_Organisation"

PartsFolder = RepoPath + "/1_Parts"
AssembFolder = RepoPath + "/2_Assemblies"
     

print('Working in ' + RepoPath)


CatalogjsonPath = OrgFolder + "/" + Catalogjson_file
AssemblyjsonPath = OrgFolder + "/" + Assemblies_file

if os.path.exists(CatalogjsonPath):
    with open(CatalogjsonPath,'r') as file:
        Catalog = json.load(file)
else:
    print("The Catalog json file does not exist. Please run the AIT_CatalogJSON.py script before running this one.")
    print("Stopping script.")
    sys.exit()

if os.path.exists(AssemblyjsonPath):
    with open(AssemblyjsonPath,'r') as file:
        AssemblySteps = json.load(file)
        
else:
    print("The Assembly json file does not exist. Please run the AIT_CatalogJSON.py script before running this one.")
    print("Stopping script.")
    sys.exit()



for segment in Catalog:
    
    for system in Catalog[segment]:
        
        for subsystem in Catalog[segment][system]:
            
            for part in Catalog[segment][system][subsystem]:
                
                if Catalog[segment][system][subsystem][part]['Material code']!="NaN":
                    
                    #print(Catalog[segment][system][subsystem][part]['Material code'])
                    
                    refName = Catalog[segment][system][subsystem][part]['Reference Name']
                    matcode = Catalog[segment][system][subsystem][part]['Material code']
                    syscode = Catalog[segment][system][subsystem][part]['System code']
                    
                    matcode_nb = len(matcode)
                    
                    for code in range(0,matcode_nb):
                        
                        if matcode[code]!="NaN":
                            
                            partPath = PartsFolder + "/" +refName + "_" + str(matcode[code]) + "_" + str(syscode)
                        
                        
                            path_nb = len(partPath)
                        
                            for path in range(0,path_nb):
                            
                                if not os.path.exists(partPath):
                                    
                                    logPath = partPath + "/1_Logs"
                                    recPath = partPath + "/2_Reception"
                                    testsPath = partPath + "/3_Tests_Asruns"
                                    anomPath = partPath + "/4_Anomalies"
                                    trendingPath = partPath + "/5_Trendings"
                                    datasPath = partPath + "/6_Datasheets"
                                    
                                    #print("Creating a folder for part " + matcode)
                                    os.makedirs(partPath)
                                    os.makedirs(logPath)
                                    os.makedirs(recPath)
                                    os.makedirs(testsPath)
                                    os.makedirs(anomPath)
                                    os.makedirs(trendingPath)
                                    os.makedirs(datasPath)
                                    print(partPath)
                                
                                if os.path.exists(partPath):
                                    
                                    logPath = partPath + "/1_Logs"
                                    recPath = partPath + "/2_Reception"
                                    testsPath = partPath + "/3_Tests_Asruns"
                                    anomPath = partPath + "/4_Anomalies"
                                    trendingPath = partPath + "/5_Trendings"
                                    datasPath = partPath + "/6_Datasheets"
                                    
                                    if not os.path.exists(logPath):
                                        os.makedirs(logPath)
                                        
                                    if os.path.exists(logPath):
                                        
                                        logbookpath = logPath + "/" + str(matcode[code]) +"_Logbook.md" 
                                        
                                        if not os.path.exists(logbookpath):
                                            
                                            with open(logbookpath,'a') as book:
                                                book.write('| Date | Time | Author | Description | id |')
                                                book.write('\n')        
                                                book.write('|:----:|:----:|:----:|----|----|')
                                                book.write('\n')
                                        
                                    
                                    
                                    
                                    if not os.path.exists(recPath):
                                        os.makedirs(recPath)
                                        
                                        
                                    if  os.path.exists(recPath):
                                        
                                        readmePath = recPath + "/README.md"
                                        
                                        if not os.path.exists(readmePath):
                                            
                                            with open(readmePath,'w') as f:
                                                f.write("Reception Control folder")
                                            
                                        
                                        recimgPath = recPath + "/img_reception"
                                        pvcePath = recPath + "/PVCE"
                                        
                                        if not os.path.exists(recimgPath):
                                            os.makedirs(recimgPath)
                                            
                                        if os.path.exists(recimgPath):
                                            
                                            readmePath = recimgPath + "/README.md"
                                            
                                            if not os.path.exists(readmePath):
                                                
                                                with open(readmePath,'w') as f:
                                                    f.write("Reception Control Images folder")
                                            
                                        if not os.path.exists(pvcePath):
                                            os.makedirs(pvcePath)
                                        
                                        if os.path.exists(pvcePath):
                                            
                                            readmePath = pvcePath + "/README.md"
                                            
                                            if not os.path.exists(readmePath):
                                                
                                                with open(readmePath,'w') as f:
                                                    f.write("Reception Control Report folder")
                                        
                                    
                                    
                                    
                                    if not os.path.exists(testsPath):
                                        os.makedirs(testsPath)
                                        
                                    if os.path.exists(testsPath):
                                        
                                        readmePath = testsPath + "/README.md"
                                        
                                        if not os.path.exists(readmePath):
                                            
                                            with open(readmePath,'w') as f:
                                                f.write("Tests Asruns folder")
                                        
                                        testsimgPath = testsPath + "/img_tests"
                                        
                                        if not os.path.exists(testsimgPath):
                                            os.makedirs(testsimgPath)
                                            
                                        if os.path.exists(testsimgPath):
                                            
                                            readmePath = testsimgPath + "/README.md"
                                            
                                            if not os.path.exists(readmePath):
                                                
                                                with open(readmePath,'w') as f:
                                                    f.write("Tests Asruns images folder")
                                        
                                        
                                    if not os.path.exists(anomPath):
                                        os.makedirs(anomPath)
                                    
                                    if os.path.exists(anomPath):
                                        
                                        readmePath = anomPath + "/README.md"
                                        
                                        if not os.path.exists(readmePath):
                                            
                                            with open(readmePath,'w') as f:
                                                f.write("Anomalies folder")
                                        
                                    if not os.path.exists(trendingPath):
                                        os.makedirs(trendingPath)
                                    
                                    if os.path.exists(trendingPath):
                                        
                                        readmePath = trendingPath + "/README.md"
                                        
                                        if not os.path.exists(readmePath):
                                            
                                            with open(readmePath,'w') as f:
                                                f.write("Anomalies folder")
                                        
                                        
                                    if not os.path.exists(datasPath):
                                        os.makedirs(datasPath)
                                    
                                    if os.path.exists(datasPath):
                                        
                                        readmePath = datasPath + "/README.md"
                                        
                                        if not os.path.exists(readmePath):
                                            
                                            with open(readmePath,'w') as f:
                                                f.write("Anomalies folder")
                                        
for steps in AssemblySteps:
    if AssemblySteps[steps]["Material code"]!="NaN":
        
        Step_refName = AssemblySteps[steps]["RefName"]
        Step_matcode = AssemblySteps[steps]["Material code"]
        
        StepPath = AssembFolder + "/" + Step_refName + "_" + str(Step_matcode)
        Step_logPath = StepPath + "/1_Logs"
        Step_AssemblyPath = StepPath + "/2_Assembly_Asrun"
        
        if not os.path.exists(StepPath):
            
            Step_logPath = StepPath + "/1_Logs"
            Step_AssemblyPath = StepPath + "/2_Assembly_Asrun"
            
            os.makedirs(StepPath)
            os.makedirs(Step_logPath)
            os.makedirs(Step_AssemblyPath)
            
        if os.path.exists(StepPath):
            
            if not os.path.exists(Step_logPath):
                os.makedirs(Step_logPath)
                
            if os.path.exists(Step_logPath):
                
                Step_logbookpath = Step_logPath + "/" + str(Step_matcode) +"_Logbook.md" 
                
                if not os.path.exists(Step_logbookpath):
                    
                    with open(Step_logbookpath,'a') as book:
                        book.write('| Date | Time | Author | Description | id |')
                        book.write('\n')        
                        book.write('|:----:|:----:|:----:|----|----|')
                        book.write('\n')
            
            if not os.path.exists(Step_AssemblyPath):
                os.makedirs(Step_AssemblyPath)
            
            if os.path.exists(Step_AssemblyPath):
                
                readmePath = Step_AssemblyPath + "/README.md"
                
                if not os.path.exists(readmePath):
                    
                    with open(readmePath,'w') as f:
                        f.write("Assembly Asrun folder")
            
    
    
    
    
    
    
    
    
    
    
    
    
    
    
                                    