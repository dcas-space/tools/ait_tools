# Written by Gerald Imhof, DCAS


#!/usr/bin/env python3

#import sys, os, argparse, pdb
import urllib3
import argparse
#import json
import os
import sys
import json
import configparser
import time
from progress.bar import IncrementalBar

# Silence the irritating insecure warnings. I'm not insecure you're insecure!
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Add the location of python-gitlab to the path so we can import it
#repo_top = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
#python_gitlab_dir = os.path.join(repo_top, "external/python-gitlab")
#sys.path.append(python_gitlab_dir)
import gitlab

class CommitInfo():
        def getinfo(authkey, ServUrl, ProjName):
            
            # This function collects all the infos from all commits performed on the project, 
            # Outputs are the nb of commits made and the actual commit infos as objects. 
            
            authkey = authkey
            url = ServUrl
            project = ProjName
            
            server = gitlab.Gitlab(url, authkey, api_version=4, ssl_verify=False)
            project = server.projects.get(project)
            
            cmmts = project.commits.list(all=True)
            cmmts_all = cmmts
            nb_cmmts = len(cmmts)
            
            
            return nb_cmmts, cmmts_all
       
if __name__ == '__main__':
    
    # Parser to select the project config file to work from.

    parser = argparse.ArgumentParser()

    parser.add_argument('-p',
                        "--Project",
                        help="Entre the project's name. The config file of this project should be created before executing this script and named 'YourProject_config.ini'. ")
    parser.add_argument('-t',
                        "--startTime",
                        default='2022-03-30',
                        help="Enter the date at which you want the scan to start 'YYYY-MM-DD'. Default is 2022-03-30. It is recommended, to shorten the execution time. Beware that if a log is missed, the next time it will be added will cause it to be in the wrong place in the logbook.")
    
    parser.add_argument('-f',
                        "--forceCompleteScan",
                        help="Optional. [y] or [n]. Forcing the script to scan all commits will take a while. Take a coffee or go for a walk while it's executing")
    
    args = parser.parse_args()

    # Import project info from config file

    config_obj = configparser.ConfigParser()

    config_obj.read((args.Project+"-ait_config.ini"))
    projInfo = config_obj["project_info"]
    projFiles = config_obj["project_files"]

    ServUrl = projInfo["ServUrl"]
    ProjName = projInfo["ProjName"]
    RepoPath = projInfo["LocalRepoPath"]
    Authkey = projInfo["Authkey"]

    Catalogxlsx_file = projFiles["Catalog"]
    Catalogjson_file = projFiles["CatalogJSON"]


    ScriptPath = os.path.realpath(__file__)


    OrgFolder = RepoPath + "/0_Organisation"

    PartsFolder = RepoPath + "/1_Parts"
    
    
    # Default options for the creme-ait-test project. To be changed when you apply the script on another project or server.
    
    #ServUrl = 'https://gitlab.isae-supaero.fr'
    
    #ProjName = 'creme/creme-ait'
    
    # These are the document types associated to AIT. More can be added or changed for any needs.
    
    docTypes = ["Logbook","PVCE","Equipment_validation","Trending","NCA","RecepIMG"]
    nbdocTypes = len(docTypes)
    
    # Start date of the scan
    
    startDate = args.startTime
    
    forceComplete = args.forceCompleteScan
    

    print('Working in ' + OrgFolder + ' and ' + PartsFolder)


    jsonPath = OrgFolder + "/" + Catalogjson_file
    
    # Fetching tag list
    
    #taglistExists = os.path.exists('Creme_Catalog.json')
    
    if jsonPath==False:
        
        print("The json file containing the material codes, part refs and system codes does not exist. Please create one under the .json format by running the AIT_CatalogJSON.py script.")
        
        sys.exit()
    
    else:
        with open(jsonPath,'r') as file:
            Catalog = json.load(file)

    
    matcode=[]
    partref=[]
    partsys=[]
        
    for segment in Catalog:
        
        for system in Catalog[segment]:
            
            for subsystem in Catalog[segment][system]:
                
                for part in Catalog[segment][system][subsystem]:
                    
                    for codes in range(0,len(Catalog[segment][system][subsystem][part]['Material code'])):
                        
                        if Catalog[segment][system][subsystem][part]['Material code'][codes]!="NaN":
                            
                            matcode.append(Catalog[segment][system][subsystem][part]['Material code'][codes])
                            partref.append(Catalog[segment][system][subsystem][part]['Reference Name'])
                            partsys.append(Catalog[segment][system][subsystem][part]['System code'])
    
    nb_parts = len(matcode)
        
    
    print("Fetching the commit infos.")
    
    (nb_cmmts, cmmts_all) = CommitInfo.getinfo(Authkey, ServUrl, ProjName)
        
    print("Fetched")
     
    if forceComplete=='y':
        
        startidx = nb_cmmts-1
        
    else:    
        for idx in range(0,nb_cmmts):
            
            if cmmts_all[idx].attributes['created_at'][0:10] >= startDate:
                
                startidx = idx
                
    
    print("Cycling through the commited files.")

    time.sleep(1)
    
    bar = IncrementalBar('Commits', max = startidx)    
    
    for cmmt in range(0,startidx):
        
        bar.next()
        
        nbFilesinCommit = len(cmmts_all[cmmt].diff(all=True))
            
        for nbFile in range(0,nbFilesinCommit):
        
            for part in range(0,nb_parts):
                
                if str(matcode[part]) in cmmts_all[cmmt].diff(all=True)[nbFile]['new_path']:
                        
                    code = matcode[part]
                    name = partref[part]
                    sys = partsys[part]
                    
                    logbook_name = PartsFolder + "/" + name + "_" + str(code) + "_" + sys + "/1_Logs/" + str(code) +"_Logbook.md"
                    
                    logbookExists = os.path.exists(logbook_name)
                    
                    if logbookExists==False:
                        print("The logbook for the part " + code + " does not exist. Please create one following the convention 'MaterialCode_Logbook.md'")
                                    
                        sys.exit()
                    
                    if cmmts_all[cmmt].diff(all=True)[nbFile]['new_file']==False and len(cmmts_all[cmmt].diff(all=True)[nbFile]['diff'])==0:
                        
                        pass
                    
                    else:
                        
                        for doc in range(0,nbdocTypes):
                            
                            if docTypes[doc] in cmmts_all[cmmt].diff(all=True)[nbFile]['new_path'][::-1].split('/')[0][::-1]:
                                
                                if 'Logbook' in cmmts_all[cmmt].diff(all=True)[nbFile]['new_path'][::-1].split('/')[0][::-1] and cmmts_all[cmmt].diff(all=True)[nbFile]['new_file']==False:
                                
                                    #print("The document type is a(n) " + docTypes[doc])
                                    pass
                                
                                elif '.zip' in cmmts_all[cmmt].diff(all=True)[nbFile]['new_path'][::-1].split('/')[0][::-1]:
                                    
                                    pass
                                
                                else:
                                    with open(logbook_name) as f:
                                        
                                        if cmmts_all[cmmt].get_id() in f.read():
                                            
                                            pass
                                        
                                        else:    
                                            print(" Adding a new entry log to part ",code, " Logbook.")
                                                
                                            with open(logbook_name,'a') as t:
                                                t.write("|")
                                                t.write(cmmts_all[cmmt].attributes['committed_date'][0:10])
                                                t.write("|")
                                                t.write(cmmts_all[cmmt].attributes['committed_date'][11:19])
                                                t.write("|")
                                                t.write(cmmts_all[cmmt].attributes['author_name'])
                                                t.write("|")
                                                if cmmts_all[cmmt].diff(all=True)[nbFile]['new_file']==True:
                                                    t.write("Created ")
                                                else:
                                                    t.write("Modified ")
                                                t.write("part ")
                                                t.write(str(code))
                                                t.write(" ")
                                                t.write(docTypes[doc])
                                                t.write("|")
                                                t.write(cmmts_all[cmmt].attributes['id'])
                                                t.write("|")
                                                t.write('\n')
    bar.finish()
    time.sleep(1)
    print("Logbooks are complete.")
        
                            