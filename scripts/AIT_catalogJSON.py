# Written by Gerald Imhof, DCAS

import argparse
import math
import os
import pandas as pd
import json
import configparser
import numpy


# Parser to select the project config file to work from

parser = argparse.ArgumentParser()

parser.add_argument('-p',
                    "--Project",
                    help="Enter the project's name. The config file of this project should be created before executing this script and named 'YourProject-ait_config.ini'. ")

args = parser.parse_args()

# Import project info from config file

config_obj = configparser.ConfigParser()

config_obj.read((args.Project+"-ait_config.ini"))
projInfo = config_obj["project_info"]
projFiles = config_obj["project_files"]

ServUrl = projInfo["ServUrl"]
ProjName = projInfo["ProjName"]
RepoPath = projInfo["LocalRepoPath"]
Authkey = projInfo["Authkey"]

Catalogxlsx_file = projFiles["Catalog"]
Catalogjson_file = projFiles["CatalogJSON"]
Assemblyjson_file = projFiles["Assemblies"]
aitDatajson_file = projFiles["AitJSON"]
aitSuccjson_file = projFiles["AitSuccessorJSON"]

ScriptPath = os.path.realpath(__file__)

print('Working in ' + RepoPath)

OrgFolder = RepoPath + "/0_Organisation"

xlsx_file = OrgFolder + "/" + Catalogxlsx_file


sheet_xlsx = pd.read_excel(xlsx_file)

Cols = sheet_xlsx.columns
col_nb = len(Cols)

MatCodeCols=[]

for col in range(0,col_nb):
    
    if type(Cols[col]) is str: 
        if Cols[col][0:13]=="Material_code":
            MatCodeCols.append(Cols[col])

matcode_nb = len(MatCodeCols)


AIT_interf = {}
                                                
for step in range(0,len(sheet_xlsx.Steps)):
    if type(sheet_xlsx.Steps[step]) is str:
        
        AIT_interf[sheet_xlsx.Steps[step]]={
            #"state": sheet_xlsx.Step_state[step],
            
            "Equipement Validation":{},
            
            "Assembly":{
                
            "path": sheet_xlsx.Step_folder[step],
            "state": sheet_xlsx.Step_Assembly[step]
            
            },
            
            "Health Check":{
            
            "path": sheet_xlsx.Step_folder[step],
            "state": sheet_xlsx.Step_Health[step]
            }
            }


AIT_Successor = {}

for step in range(0,len(sheet_xlsx.Steps)):
    if type(sheet_xlsx.Steps[step]) is str:
        
        AIT_Successor[sheet_xlsx.Steps[step]]={
            "successor": sheet_xlsx.Step_successor[step],
            }

AIT_Assemblies = {}


for step in range(0,len(sheet_xlsx.Steps)):
    if type(sheet_xlsx.Steps[step]) is str:
    
        if not numpy.isnan(sheet_xlsx.Assemb_Material_code[step]):
            AIT_Assemblies[sheet_xlsx.Steps[step]]={
                "Material code": int(sheet_xlsx.Assemb_Material_code[step]),
                "RefName": sheet_xlsx.Step_RefName[step]

                }
        else:
            AIT_Assemblies[sheet_xlsx.Steps[step]]={
                "Material code": "NaN"

                }


for assembly in range(0,len(sheet_xlsx.Steps)):
    if type(sheet_xlsx.Steps[assembly]) is str:
        
        parts_instep = []
        
        
        for part in range(0,len(sheet_xlsx.Part)):
            if type(sheet_xlsx.AIT_step[part]) is str:
                
                aitstep = sheet_xlsx.AIT_step[part]
                
            else:
                
                aitstep = "NaN"
            
            parts_codes = []
            
            if aitstep == sheet_xlsx.Steps[assembly]:
                
                #print(aitstep)
                #parts_instep.append(sheet_xlsx.Part[part])
                
                for MatCodeCol in range(0,matcode_nb):
                    
                    if type(getattr(sheet_xlsx,MatCodeCols[MatCodeCol])[part]) is numpy.float64:
                        #print(getattr(sheet_xlsx,MatCodeCols[MatCodeCol])[part])
                        
                        if not numpy.isnan(getattr(sheet_xlsx, MatCodeCols[MatCodeCol])[part]):
                            
                            parts_codes.append(int(getattr(sheet_xlsx, MatCodeCols[MatCodeCol])[part]))
                        
                    if type(sheet_xlsx.State[part]) is not str:
                            
                        part_state = "NaN"
                            
                    if type(sheet_xlsx.State[part]) is str:
                        
                        part_state = sheet_xlsx.State[part]
                            
                
                
                AIT_Assemblies[sheet_xlsx.Steps[assembly]][sheet_xlsx.Part[part]]={
                    "Material codes": parts_codes,
                    "State": part_state
                    }



Catalog = {}


for segment in range(0,len(sheet_xlsx.Segment)):
    if type(sheet_xlsx.Segment[segment]) is str:
        
        Catalog[sheet_xlsx.Segment[segment]]={}
        
        for system in range(0,len(sheet_xlsx.System)):
            if type(sheet_xlsx.System[system]) is str:
                
                Catalog[sheet_xlsx.Segment[segment]][sheet_xlsx.System[system]] = {}
                
                for subsystem in range(0,len(sheet_xlsx.Subsystem)):
                    if type(sheet_xlsx.Subsystem[subsystem]) is str:
                        
                        if sheet_xlsx.Subsystem[subsystem][0:2]==sheet_xlsx.System[system][0:2]:
                            Catalog[sheet_xlsx.Segment[segment]][sheet_xlsx.System[system]][sheet_xlsx.Subsystem[subsystem]]={}
                        
                            for part in range(0,len(sheet_xlsx.Part)):
                                if type(sheet_xlsx.System_code[part]) is str:
                                
                                    if sheet_xlsx.System_code[part][0:3]==sheet_xlsx.Subsystem[subsystem][0:3]:
                                        
                                        if math.isnan(sheet_xlsx.Amount[part]):
                                            amount = "NaN"
                                        else:
                                            amount = int(sheet_xlsx.Amount[part])
                                        
                                        if type(sheet_xlsx.State[part]) is not str:
                                            state = "NaN"
                                        else:
                                            state = sheet_xlsx.State[part]
                                            
                                        if type(sheet_xlsx.Location[part]) is float: 
                                            if math.isnan(sheet_xlsx.Location[part]):
                                                location = "NaN"
                                        else:
                                            location = sheet_xlsx.Location[part]
                                        
                                        if type(sheet_xlsx.AIT_step[part]) is float:
                                            if math.isnan(sheet_xlsx.AIT_step[part]):
                                                aitstep = "NaN"
                                        else:
                                            aitstep = sheet_xlsx.AIT_step[part]
                                        
                                        matcode=[]
                                        
                                        for MatCodeCol in range(0,matcode_nb):
                                            #if type(sheet_xlsx.Material_code[part]) is float:
                                                if math.isnan(getattr(sheet_xlsx, MatCodeCols[MatCodeCol])[part]):
                                                    matcode.append("NaN") 
                                                
                                                else:
                                                    matcode.append(int(getattr(sheet_xlsx, MatCodeCols[MatCodeCol])[part]))
                                        
                                        
                                        if type(sheet_xlsx.Reference_Name[part]) is not str:
                                            if math.isnan(sheet_xlsx.Reference_Name[part]):
                                                refName = "NaN"
                                        else:
                                            refName = sheet_xlsx.Reference_Name[part]
                                            
                                        # if type(sheet_xlsx.Date_ordered[part]) is not str:
                                        #     if math.isnan(sheet_xlsx.Date_ordered[part]):
                                        #         dateOrdered = "NaN"
                                        # else:
                                        #     dateOrdered = str(sheet_xlsx.Date_ordered[part])
                                        
                                        # if type(sheet_xlsx.Date_received[part]) is not str:
                                        #     if math.isnan(sheet_xlsx.Date_received[part]):
                                        #         dateRec = "NaN"
                                        # else:
                                        #     dateRec = sheet_xlsx.Date_received[part]
                                        
                                        if type(sheet_xlsx.Folder[part]) is not str:
                                            if math.isnan(sheet_xlsx.Folder[part]):
                                                folder = "NaN"
                                        else:
                                            folder = sheet_xlsx.Folder[part]
                                            
                                        
                                        Catalog[sheet_xlsx.Segment[segment]][sheet_xlsx.System[system]][sheet_xlsx.Subsystem[subsystem]][
                                            sheet_xlsx.Part[part]]={
                                            "System code": sheet_xlsx.System_code[part],
                                            "Amount": amount,
                                            "State": state,
                                            "Location": location,
                                            "AIT step": aitstep,
                                            "Material code": matcode,
                                            "Reference Name": refName,
                                            # "Date ordered": dateOrdered,
                                            # "Date received": dateRec,
                                            "Datasheet": folder
                                            
                                            }
                                                
                                        for block in range(0,len(AIT_interf)):
                                            if aitstep == list(dict.keys(AIT_interf))[block]:
                                                
                                                
                                                AIT_interf[list(dict.keys(AIT_interf))[block]]["Equipement Validation"][sheet_xlsx.Part[part]]={
                                                    
                                                    "path": folder,
                                                    "state": state,
                                                    "name": refName
                                                }


                                            
                                          
aitSucc = json.dumps(AIT_Successor)
aitSuccPath = OrgFolder + "/" + aitSuccjson_file

aitJSON = json.dumps(AIT_interf)
aitjsonPath = OrgFolder + "/" + aitDatajson_file
                                                
CatalogJSON = json.dumps(Catalog)
CatalogjsonPath = OrgFolder + "/" + Catalogjson_file

AssemblyJSON = json.dumps(AIT_Assemblies)
AssemblyjsonPath = OrgFolder + "/" + Assemblyjson_file

with open (CatalogjsonPath,'w') as file:
    file.write(CatalogJSON)

with open (AssemblyjsonPath,'w') as file:
    file.write(AssemblyJSON)

with open (aitjsonPath,'w') as file:
    file.write(aitJSON)

with open(aitSuccPath,'w') as file:
    file.write(aitSucc)

print("Updated json files.")